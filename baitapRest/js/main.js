const tinhTrungBinh = (...scores) => {
    var tong = 0
    scores.forEach(val => {
        tong += val
    })
    console.log(tong)
    return tong / (scores.length)
}

const tinhDiemKhoi1 = () => {
    var inpToan = document.getElementById("inpToan").value * 1;
    var inpLy = document.getElementById("inpLy").value * 1;
    var inpHoa = document.getElementById("inpHoa").value * 1;
    document.getElementById("tbKhoi1").innerHTML = tinhTrungBinh(inpToan, inpLy, inpHoa)
}

const tinhDiemKhoi2 = () => {
    var inpVan = document.getElementById("inpVan").value * 1;
    var inpSu = document.getElementById("inpSu").value * 1;
    var inpDia = document.getElementById("inpDia").value * 1;
    var inpEnglish = document.getElementById("inpEnglish").value * 1;
    document.getElementById("tbKhoi2").innerHTML = tinhTrungBinh(inpVan, inpSu, inpDia, inpEnglish)
}
