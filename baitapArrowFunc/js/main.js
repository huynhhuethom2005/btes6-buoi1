const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

var btnColorContainer = ""

colorList.forEach(value => {
    btnColorContainer += `<button onClick="changeColor(this)" id="${value}" class="color-button ${value}" ></button>`
})

document.getElementById("colorContainer").innerHTML = btnColorContainer

const changeColor = (obj) => {
    var element = document.getElementById("house");
    element.classList.forEach(el => {
        if (el != "house") {
            element.classList.remove(el);
        }
    })
    element.classList.add(obj.id);
}


